var verbs = [];
var nouns = [];
var adjectives = [];

var askWord = function(type, arr) {
  // type is one of these strings: "verb", "noun", "adjective"
  // arr is a reference to verbs, nouns, or adjectives array
  // this function push a word into the corresponding array
  return arr.push(prompt("Enter one " + type));
};

var rand = function(len) {
  // returns a number in the range [0,len-1]
  // useful to get a random word from an array
  return Math.floor(Math.random() * len);
};

var getWord = function(arr) {
  // returns a random word and remove it from the arr array
  if (arr.length) {
    return arr.splice(rand(arr.length), 1);
  } else {
    return "mmm...";
  };
};

var puzzleIt = function() {
  var output;
  output = "I like to " + getWord(verbs) + ". My favorite " + getWord(nouns);
  output += " is " + getWord(adjectives) + " " + getWord(nouns) + ".";
  output += " That's what makes me " + getWord(adjectives) + ".";
  return output;
};

var start = function() {
  askWord("verb", verbs);
  askWord("noun", nouns);
  askWord("noun (yes another one)", nouns);
  askWord("adjective", adjectives);
  askWord("adjective (the last one)", adjectives);
  alert(puzzleIt());
  return;
};

start();
